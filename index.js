const express = require('express');
const Datastore = require('nedb');
const app = express();
app.listen(3000, () => console.log('listening at 3000'));
app.use(express.static('public'))
app.use(express.json({limit: '100mb'}));

const englishDatabase = new Datastore('EnglishDatabase.db');
englishDatabase.loadDatabase();
const frenchDatabase = new Datastore('FrenchDatabase.db');
frenchDatabase.loadDatabase();
const germanDatabase = new Datastore('GermanDatabase.db');
germanDatabase.loadDatabase();
const italianDatabase = new Datastore('ItalianDatabase.db');
italianDatabase.loadDatabase();
const romanianDatabase = new Datastore('RomanianDatabase.db');
romanianDatabase.loadDatabase();
const dutchDatabase = new Datastore('DutchDatabase.db');
dutchDatabase.loadDatabase();
const imageDatabase = new Datastore('ImageDatabase.db');
imageDatabase.loadDatabase();


app.get('/english', (request, response) => 
{
    englishDatabase.find({}, (err, data) => 
    {
        response.json(data);
    });
}
);

app.get('/french', (request, response) => 
{
    frenchDatabase.find({}, (err, data) => 
    {
        response.json(data);
    });
}
);

app.get('/german', (request, response) => 
{
    germanDatabase.find({}, (err, data) => 
    {
        response.json(data);
    });
}
);

app.get('/italian', (request, response) => 
{
    italianDatabase.find({}, (err, data) => 
    {
        response.json(data);
    });
}
);

app.get('/romanian', (request, response) => 
{
    romanianDatabase.find({}, (err, data) => 
    {
        response.json(data);
    });
}
);

app.get('/dutch', (request, response) => 
{
    dutchDatabase.find({}, (err, data) => 
    {
        response.json(data);
    });
}
);

app.get('/image', (request, response) => 
{
    imageDatabase.find({}, (err, data) => 
    {
        response.json(data);
    });
}
);

app.post('/english', (request, response) => 
{

    console.log('I got an entry in the English database!');
    
    const data = request.body;
    
    englishDatabase.insert(data);
    response.json(data);

}
);

app.post('/french', (request, response) => 
{

    console.log('I got an entry in the French database!');
    
    const data = request.body;
    
    frenchDatabase.insert(data);
    response.json(data);

}
);

app.post('/german', (request, response) => 
{

    console.log('I got an entry in the German database!');
    
    const data = request.body;
    
    germanDatabase.insert(data);
    response.json(data);

}
);

app.post('/italian', (request, response) => 
{

    console.log('I got an entry in the Italian database!');
    
    const data = request.body;
    
    italianDatabase.insert(data);
    response.json(data);

}
);

app.post('/romanian', (request, response) => 
{

    console.log('I got an entry in the Romanian database!');
    
    const data = request.body;
    
    romanianDatabase.insert(data);
    response.json(data);

}
);

app.post('/dutch', (request, response) => 
{

    console.log('I got an entry in the Dutch database!');
    
    const data = request.body;
    
    dutchDatabase.insert(data);
    response.json(data);

}
);

app.post('/image', (request, response) => 
{

    console.log('I got an image entry in the database!');
    
    const data = request.body;
    
    imageDatabase.insert(data);
    response.json(data);

}
);