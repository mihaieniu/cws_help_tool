function setup()
{
    var headingNumber = 1;
    var dropdownCollection = ["Choose Category", "Sequencer", "Audio", "Granular", "Effects"];
    var languageCollection = ["Choose Language", "English", "French", "German", "Italian", "Romanian", "Dutch"];
    var languageApiCollection = ['/api', '/english', '/french', '/german', '/italian', '/romanian', '/dutch'];
    

    function numberIncrement()
    {
        
        ++headingNumber;
        return headingNumber;
        
    }

    var HtmlElement = document.getElementById("heading1");

    function assignAppendTarget(appendTarget)
    {
        if (appendTarget == null)
        {
            return;
        }
        else
        {
            HtmlElement = appendTarget;
        }
    }


    var sketch = function(p5)
            {
                let parentToAttach;
                let img;
                let fileData;


                p5.parentToAppendTo = function(parent)
                {
                    parentToAttach = parent;
                    return parentToAttach;
                };
                p5.handleFile = function(file)
                {
                    if (file.type === 'image') 
                    {
                        img = null;
                        img = p5.createImg(file.data);
                        var spacer = document.createElement('div');
                        spacer.innerHTML=`<br></br>`
                        parentToAttach.appendChild(spacer);
                        img.parent(parentToAttach);
                        fileData = file.data;
                    } 
                    else 
                    {
                        img = null;
                    }
                };
                p5.saveDataToArray = function()
                {
                    if(fileData == null)
                    {
                        return "no Image";
                    }
                    else
                    {
                    return fileData;
                    }
                };
            }

    assignAppendTarget();

    

class HtmlHeading
{
    constructor()
    {
            this.fileData;
            this.createCategoryDropdown();            
    }

    createCategoryDropdown()
    {
        if (headingNumber == 1)
            {
                this.dropdown = document.createElement('select');
                this.dropdownAtt = document.createAttribute("id");
                this.dropdownAtt.value="CategoryDropdown";
                this.dropdown.setAttributeNode(this.dropdownAtt);
                for (var i = 0; i < dropdownCollection.length; ++i)
                {
                    
                    const dropdownEntry = document.createElement('option');
                    const dropdownEntryAtt = document.createAttribute("value");
                    dropdownEntryAtt.value=dropdownCollection[i];
                    dropdownEntry.setAttributeNode(dropdownEntryAtt);
                    dropdownEntry.text=dropdownCollection[i];
                    this.dropdown.add(dropdownEntry);

                }
                HtmlElement.appendChild(this.dropdown);



                const spacer3 = document.createElement('div');
                spacer3.innerHTML=`<br></br>`
                HtmlElement.appendChild(spacer3);

                this.dropdown.addEventListener('change', async event =>
                {
                    this.createLanguageDropdown();
                    return;
                });

            }
        else
            {
                this.createLanguageDropdown();
                return;
            }
    }

    createLanguageDropdown()
    {
        if (headingNumber == 1)
            {
                this.languageDropdown = document.createElement('select');
                this.languageDropdownAtt = document.createAttribute("id");
                this.languageDropdownAtt.value="LanguageDropdown";
                this.languageDropdown.setAttributeNode(this.languageDropdownAtt);
                for (var i = 0; i < languageCollection.length; ++i)
                {
                    
                    const languageDropdownEntry = document.createElement('option');
                    const languageDropdownEntryAtt = document.createAttribute("value");
                    languageDropdownEntryAtt.value=languageCollection[i];
                    languageDropdownEntry.setAttributeNode(languageDropdownEntryAtt);
                    languageDropdownEntry.text=languageCollection[i];
                    this.languageDropdown.add(languageDropdownEntry);

                }
                HtmlElement.appendChild(this.languageDropdown);



                const spacer0 = document.createElement('div');
                spacer0.innerHTML=`<br></br>`
                HtmlElement.appendChild(spacer0);

                this.languageDropdown.addEventListener('change', async event =>
                {
                    this.createHtmlHeading();
                    return;
                });
            }
        else
            {
                this.createHtmlHeading();
                return;
            }
    }


    createHtmlHeading()
    {
        
            this.titleLabel = document.createElement('label');
            const titleLabelAtt1 = document.createAttribute("id");
            titleLabelAtt1.value="this.titleLabel"+headingNumber;
            this.titleLabel.setAttributeNode(titleLabelAtt1);
            const titleLabelAtt2 = document.createAttribute("value");
            titleLabelAtt2.value="Heading"+headingNumber;
            this.titleLabel.setAttributeNode(titleLabelAtt2);
            this.titleLabel.innerHTML=
            `Enter Title
            `
            HtmlElement.appendChild(this.titleLabel);
            
            
        
            this.headingTitle = document.createElement('input')
            this.headingTitleAtt1 = document.createAttribute("id");
            this.headingTitleAtt1.value = "headingTitle"+headingNumber;
            this.headingTitle.setAttributeNode(this.headingTitleAtt1);
            
            HtmlElement.appendChild(this.headingTitle);

            this.spacer1 = document.createElement('div');
            this.spacer1.innerHTML=`<br></br>`
            HtmlElement.appendChild(this.spacer1);

            

            this.contentLabel = document.createElement('label');
            const contentLabelAtt1 = document.createAttribute("id");
            contentLabelAtt1.value="this.titleLabel"+headingNumber;
            this.contentLabel.setAttributeNode(contentLabelAtt1);
            const contentLabelAtt2 = document.createAttribute("value");
            contentLabelAtt2.value="Heading"+headingNumber;
            this.contentLabel.setAttributeNode(contentLabelAtt2);
            this.contentLabel.innerHTML=
            `Enter Content
            `
            HtmlElement.appendChild(this.contentLabel);

            this.textInput = document.createElement('textarea');
            this.textInputAtt1 = document.createAttribute("id");
            this.textInputAtt1.value="headingEntry"+headingNumber;
            this.textInput.setAttributeNode(this.textInputAtt1);

            HtmlElement.appendChild(this.textInput);

            this.spacer2 = document.createElement('div');
            this.spacer2.innerHTML=`<br></br>`
            this.spacer2Att = document.createAttribute("id");
            this.spacer2Att.value = "spacer2"+headingNumber;
            this.spacer2.setAttributeNode(this.spacer2Att);
            HtmlElement.appendChild(this.spacer2);

            

            var myp5 = new p5(sketch);
            var imageParent = myp5.parentToAppendTo(this.spacer2);

            
            this.chooseImageButton = createFileInput(myp5.handleFile);
            this.chooseImageButton.parent(this.spacer2);
            this.chooseImageButton.id('chooseImage');

            const spacer3 = document.createElement('div');
            spacer3.innerHTML=`<br></br>`
            this.spacer2.appendChild(spacer3);

            this.loadImageButton = document.createElement('button');
            const loadImageButtonAtt = document.createAttribute("id");
            loadImageButtonAtt.value = "loadImageButton"+headingNumber;
            this.loadImageButton.setAttributeNode(loadImageButtonAtt);
            this.loadImageButton.innerHTML=`Load Image to Database`;

            this.spacer2.appendChild(this.loadImageButton);

            this.loadImageButton.addEventListener('click', async event => 
            { 
                this.fileData = myp5.saveDataToArray(); 

            });


            this.spacer4 = document.createElement('div');
            this.spacer4.innerHTML=`<br></br>`
            HtmlElement.appendChild(this.spacer4);

            this.removeHeadingButton = document.createElement('button');
            const removeHeadingButtonAtt = document.createAttribute("id");
            removeHeadingButtonAtt.value = "removeHeadingButton"+headingNumber;
            this.removeHeadingButton.setAttributeNode(removeHeadingButtonAtt);
            this.removeHeadingButton.innerHTML=`Remove Heading`

            HtmlElement.appendChild(this.removeHeadingButton);

            this.spacer5 = document.createElement('div');
            this.spacer5.innerHTML=`<br></br>`
            HtmlElement.appendChild(this.spacer5);


            numberIncrement();

            this.removeHeadingButton.addEventListener('click', async event =>
            {
                
                HtmlElement.removeChild(this.headingTitle);
                HtmlElement.removeChild(this.titleLabel);
                HtmlElement.removeChild(this.contentLabel);
                HtmlElement.removeChild(this.textInput);
                HtmlElement.removeChild(this.spacer1);
                HtmlElement.removeChild(this.spacer4);
                HtmlElement.removeChild(this.spacer5);
                HtmlElement.removeChild(this.spacer2);
                HtmlElement.removeChild(this.removeHeadingButton);
                --headingNumber;
                delete(this);
            });


    }

    outputImage()
    {
        if (this.fileData != null)
        {
            return this.fileData;
        }
        else
        {
            return "no Image";
        }
    
    }

    outputData()
    {        
        var headingOutput = [];

        let titleValue = this.headingTitle.value;
        if (titleValue != "")
        {
            headingOutput.push(titleValue);
        }
        else 
        {
            alert("Title " + instanceNumber + " is Empty");
            return;
        }

        let contentValue = this.textInput.value;
        if (contentValue != "")
        {
            headingOutput.push(contentValue);
        }
        else
        {
            alert("Content " + instanceNumber + " is Empty");
            return;
        }


        return headingOutput;
    }


}



class headingManager
{
        constructor()
        {
            this.headingCollection = [];
            this.addHeading();
            this.outputArray = [];
            this.imageArray = [];
            this.output = {};
            this.imageOutput = {};
        }

        addHeading(){


            this.headingCollection.push(new HtmlHeading());

            
            
        }
        submitData(){

            
            for (var i = 0; i < this.headingCollection.length; ++i)
            {
                this.outputArray.push(this.headingCollection[i].outputData());
            };

            let DatabaseCategory = document.getElementById("CategoryDropdown").value;

            var DatabaseEntry = this.outputArray;
            this.output = {DatabaseCategory, DatabaseEntry};
            
            return this.output;

        }
        submitImages(){

            
            for (var i = 0; i < this.headingCollection.length; ++i)
            {
                this.imageArray.push(this.headingCollection[i].outputImage());
            };

            let DatabaseCategory = document.getElementById("CategoryDropdown").value;

            var DatabaseEntry = this.imageArray;
            this.imageOutput = {DatabaseCategory, DatabaseEntry};

            
            return this.imageOutput;

        }

    };

    const manager = new headingManager();

    const addButton = document.getElementById('addHeading');
    addButton.onclick = function()
    {
        manager.addHeading();
    };

    const submitButton = document.getElementById('submit');
    submitButton.addEventListener('click', async event => 
    {
        let languageDropdownValue = document.getElementById('LanguageDropdown').value;
        let languageDropdownIndex = document.getElementById('LanguageDropdown').selectedIndex;

        if (languageDropdownValue == languageCollection[0])
        {
            alert("Language not selected");
            return;
        }
        else
        {
        
            const dataOptions = 
            {
                
                method: 'POST',
                headers: 
                {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(manager.submitData())
            };


            
            const response = await fetch(languageApiCollection[languageDropdownIndex], dataOptions);
            const json = await response.json();
            console.log("Logged in " + languageCollection[languageDropdownIndex] + "Database");
        }
    });
    submitButton.addEventListener('click', async event => 
    {
        let languageDropdownValue = document.getElementById('LanguageDropdown').value;

        if (languageDropdownValue == languageCollection[0])
        {
            alert("Language not selected");
            return;
        }
        else
        {
        
            const dataOptions = 
            {
                
                method: 'POST',
                headers: 
                {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(manager.submitImages())
            };


            
            const response = await fetch('/image', dataOptions);
            const json = await response.json();
            console.log("Logged in images Database");
        }
});


}
