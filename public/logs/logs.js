getData();

async function getData() {
  
  const options = {
    method: 'get',
    headers: {
      'Content-Type': 'application/json'
    }
    //body: JSON.stringify(data)
  };
  const response = await fetch('/api', options);
  const data = await response.json();
  console.log(data);
  
  
  

  for (item of data) {
    const root = document.createElement('p');
    const mood = document.createElement('div');
    const geo = document.createElement('div');
    const date = document.createElement('div');
    const image = document.createElement('img');

    root.remove();
    
    mood.textContent = `mood: ${item.mood}`;
    
    geo.textContent = `${item.lat}°, ${item.lon}°`;
    const dateString = new Date(item.timestamp).toLocaleString();
    date.textContent = dateString;
    image.src = item.image64;
    image.alt = 'Dan Shiffman making silly faces.';
    
    if (`mood: ${item.mood}` != undefined)
    {
      root.append(mood);
      console.log("added mood");
    }
    else
    {
      root.remove(mood);
    }
    if (`${item.lat}°, ${item.lon}°` != null)
    {
      root.append(geo);
    }
    else
    {
      root.remove(geo);
    }
    if (dateString != null)
    {
      root.append(date)
    }
    else
    {
      root.remove(date);
    }
    if (item.image64 != null)
    {
      root.append(image);
    }
    else
    {
      root.remove(image);
    }
    //root.append(mood, geo, date, image);
    document.body.append(root);
  }
};
