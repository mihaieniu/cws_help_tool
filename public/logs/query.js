function setup()
{
    
let englishData;
let frenchData;
let germanData;
let italianData;
let romanianData;
let dutchData;
let imageData;

getEnglishData();
getFrenchData();
getGermanData();
getItalianData();
getRomanianData();
getDutchData();
getImages();

var sketch = function(p5)
{
    let parentToAttach;
    let img;


    p5.parentToAppendTo = function(parent)
    {
        parentToAttach = parent;
        return parentToAttach;
    };
    p5.loadMyImage = function(file)
    {
        if (file != null) 
        {
            img = null;
            img = p5.createImg(file);
            img.id('images');
            img.parent(parentToAttach);
        } 
        else 
        {
            return;
        }
    };
                            
}

function compareValues(key, order='asc') 
{
    return function(a, b) 
    {
      if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) 
      {
        // property doesn't exist on either object
        return 0;
      }
  
      const varA = (typeof a[key] === 'string') ?
        a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string') ?
        b[key].toUpperCase() : b[key];
  
      let comparison = 0;
      if (varA > varB) 
      {
        comparison = 1;
      } else if (varA < varB) 
      {
        comparison = -1;
      }
      return (
        (order == 'desc') ? (comparison * -1) : comparison
      );
    };
  }
                          
                          



async function getEnglishData() 
{
    
    const options = 
    {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
        //body: JSON.stringify(data)
    };
    const response = await fetch('/english', options);
    const data = await response.json();

    data;

    if (data != null)
    {
        englishData = data;
        englishData.sort(compareValues('DatabaseCategory'));
        console.log("English loaded")
        console.log(englishData);
    }

};

async function getFrenchData() 
{
    
    const options = 
    {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
        //body: JSON.stringify(data)
    };
    const response = await fetch('/french', options);
    const data = await response.json();

    data;

    if (data != null)
    {
        frenchData = data;
        frenchData.sort(compareValues('DatabaseCategory'));
        console.log("French loaded");
        console.log(frenchData);
    }

};

async function getGermanData() 
{
    
    const options = 
    {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
        //body: JSON.stringify(data)
    };
    const response = await fetch('/german', options);
    const data = await response.json();

    data;

    if (data != null)
    {
        germanData = data;
        germanData.sort(compareValues('DatabaseCategory'));
        console.log("German loaded");
        console.log(germanData);
    }

};

async function getItalianData() 
{
    
    const options = 
    {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
        //body: JSON.stringify(data)
    };
    const response = await fetch('/italian', options);
    const data = await response.json();

    data;

    if (data != null)
    {
        italianData = data;
        italianData.sort(compareValues('DatabaseCategory'));
        console.log("Italian Loaded");
        console.log(italianData);
    }

};

async function getRomanianData() 
{
    
    const options = 
    {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
        //body: JSON.stringify(data)
    };
    const response = await fetch('/romanian', options);
    const data = await response.json();

    data;

    if (data != null)
    {
        romanianData = data;
        romanianData.sort(compareValues('DatabaseCategory'));
        console.log("romanianLoaded");
        console.log(romanianData);
    }

};

async function getDutchData() 
{
    
    const options = 
    {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
        //body: JSON.stringify(data)
    };
    const response = await fetch('/dutch', options);
    const data = await response.json();

    data;

    if (data != null)
    {
        dutchData = data;
        dutchData.sort(compareValues('DatabaseCategory'));
        console.log("Dutch loaded");
        console.log(dutchData);
    }

};

async function getImages()
{
    const options =
        {
            method: 'get',
            headers: 
            {
                'Content-Type': 'application.json'
            }
        };
        const response = await fetch('/image', options);
        const data = await response.json();

        if (data != null)
        {
            imageData = data;
            imageData.sort(compareValues('DatabaseCategory'));
            console.log("images Loaded");
            console.log(imageData);
        }
};


function display(dataArray)
{
    var textDataArray = [];
    var imageDataArray = [];
    var contentText = [];
    var contentImage = [];

    if (document.getElementById("libraryEntry"))
    {
        document.getElementById("libraryEntry").remove();
    }
        
        for (var i = 0; i < dataArray.length; ++i)
            {

                const root = document.createElement('div');
                const rootAtt = document.createAttribute("id");
                rootAtt.value = "libraryEntry";
                root.setAttributeNode(rootAtt);
                const category = document.createElement('p');
                
                
                textDataArray = dataArray[i].DatabaseEntry;
                imageDataArray = imageData[i].DatabaseEntry;

                root.remove();
                category.textContent = `${dataArray[i].DatabaseCategory}`;

                document.getElementById("heading1").appendChild(root);
                document.getElementById("heading1").appendChild(category);
                
            
                for (var n = 0; n < textDataArray.length; ++n)
                    {
                        contentText = textDataArray[n];
                        contentImage = imageDataArray[n];
                     

                        const title = document.createElement('div');
                        const titleAtt = document.createAttribute("id");
                        titleAtt.value = "title";
                        title.setAttributeNode(titleAtt);
                        title.textContent = `${contentText[0]}`;
                        document.getElementById("heading1").appendChild(title);
                        
                        var spacer1 = document.createElement('div');
                        spacer1.innerHTML=`<br></br>`
                        document.getElementById("heading1").appendChild(spacer1);                        

                        if (dataArray[i].DatabaseCategory == imageData[i].DatabaseCategory)
                        {
                            let image = document.createElement('image');

                            var myp5 = new p5(sketch);
                            var imageParent = myp5.parentToAppendTo(document.getElementById("heading1"));
                            if (contentImage != "no Image")
                            {
                                image = myp5.loadMyImage(contentImage);
                            }
                        }

                        const content = document.createElement('div');
                        const contentAtt = document.createAttribute("id");
                        contentAtt.value = "content";
                        content.setAttributeNode(contentAtt);
                        content.textContent = `${contentText[1]}`;
                        document.getElementById("heading1").appendChild(content);
                        
                        var spacer2 = document.createElement('div');
                        spacer2.innerHTML=`<br></br>`
                        document.getElementById("heading1").appendChild(spacer2); 

                
                
               
                
                    }
             }
        
}



const englishButton = document.getElementById("english");
englishButton.addEventListener('click', async event =>
    {
        while (document.getElementById("heading1").firstChild) {
            document.getElementById("heading1").removeChild(document.getElementById("heading1").firstChild);
        }
        display(englishData);
    }
)

const frenchButton = document.getElementById("french");
frenchButton.addEventListener('click', async event =>
    {
        while (document.getElementById("heading1").firstChild) {
            document.getElementById("heading1").removeChild(document.getElementById("heading1").firstChild);
        }
        display(frenchData);
    }
)

const germanButton = document.getElementById("german");
germanButton.addEventListener('click', async event =>
    {
        while (document.getElementById("heading1").firstChild) {
            document.getElementById("heading1").removeChild(document.getElementById("heading1").firstChild);
        }
        display(germanData);
    }
)

const italianButton = document.getElementById("italian");
italianButton.addEventListener('click', async event =>
    {
        while (document.getElementById("heading1").firstChild) {
            document.getElementById("heading1").removeChild(document.getElementById("heading1").firstChild);
        }
        display(italianData);
    }
)

const romanianButton = document.getElementById("romanian");
romanianButton.addEventListener('click', async event =>
    {
        while (document.getElementById("heading1").firstChild) {
            document.getElementById("heading1").removeChild(document.getElementById("heading1").firstChild);
        }
        display(romanianData);
    }
)

const dutchButton = document.getElementById("dutch");
dutchButton.addEventListener('click', async event =>
    {
        while (document.getElementById("heading1").firstChild) {
            document.getElementById("heading1").removeChild(document.getElementById("heading1").firstChild);
        }
        display(dutchData);
    }
)

}
